from pynput.keyboard import Key, Controller, Listener
import time
from glob import glob
import argparse
keyboard = Controller()
import os.path as op

def is_end(key):
    return not key == Key.end

def run(fn, delays=(0.01, 0.5)):
    if op.isfile(fn):
        print('Found %s'%fn)
    print('Press END when ready to go...')
    with Listener(on_press=is_end) as listener:
        listener.join()

    code = open(fn).read().split('###\n')

    for part in code:
        part = part.rstrip('\n')
        lines = part.split('\n')
        for i, line in enumerate(lines):

            for char in line:
                if char == '@':
                    time.sleep(1)
                    continue
                if char == 'ç':
                    keyboard.press(Key.backspace)
                    keyboard.release(Key.backspace)
                    continue
                keyboard.press(char)
                keyboard.release(char)
                time.sleep(delays[0])

            if i < len(lines) - 1:
                keyboard.press(Key.enter)
                keyboard.release(Key.enter)
                time.sleep(delays[1])

        with keyboard.pressed(Key.shift):
            keyboard.press(Key.enter)
            keyboard.release(Key.enter)
        keyboard.press(Key.down)
        keyboard.release(Key.down)

        with Listener(on_press=is_end) as listener:
            listener.join()


if __name__=="__main__" :
    parser = argparse.ArgumentParser(description='test')
    parser.add_argument('input', help='Python script to type')
    parser.add_argument('-d', type=float, default=0.01, help='delay (ms)', required=False)
    parser.add_argument('--d1', type=float, default=0.1, help='delay (ms) (between two lines)', required=False)
    args = vars(parser.parse_args())
    run(args['input'], delays=(args['d'], args['d1']))
