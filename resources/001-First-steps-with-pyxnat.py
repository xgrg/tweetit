import pyxnat
x = pyxnat.Interface(server='https://central.xnat.org',
user='nosetests', password='nose')
@
projects = list(x.select.projects())
@print(projects[:5])
###
project = x.select.project('xnatDownload')
subjects = list(project.subjects())
@print(subjects)
###
subject_labels = [e.label() for e in subjects]
print(subject_labels)
###
scan = project.subject('sub-001').experiment('sub-001_ses-01').scan('3')
@@print(scan.attrs.get('type'))
###
resource = scan.resources().first()
f = resource.files().first()
filename = f.label()

@print(filename)
###
f.get(dest='/tmp/%s'%filename)
