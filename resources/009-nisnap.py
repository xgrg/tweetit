from nisnap.utils import aseg
print(aseg.basal_ganglia_labels)
# Just a list of labels@
###
from nisnap import snap
@snap.plot_segment('/tmp/BBRC_E00080_aparc+aseg_swapped.mgz',
bg='/tmp/BBRC_E00080_nu_swapped.mgz',
labels=aseg.basal_ganglia_labels,
axes='x',
opacity=35,
slices=range(173,185,3),
samebox=True,
animated=True)

@# Animated snapshot of FreeSurfer's aseg results
###
from nisnap import snap

@seg_fp = '/tmp/BBRC_E00080_ASHS_left_lfseg_corr_nogray.nii.gz'
@snap.plot_segment(seg_fp,
bg='/tmp/BBRC_E00080_ASHS_tse.nii.gz',
axes='x', @slices=range(10,26), @rowsize=4,
@opacity=30)

@# Static snapshot from hippocampal subfield segmentation
###
filepaths = ['/tmp/BBRC_E00080_SPM12_SEGMENT_c1.nii.gz',
'/tmp/BBRC_E00080_SPM12_SEGMENT_c2.nii.gz',
'/tmp/BBRC_E00080_SPM12_SEGMENT_c3.nii.gz']

@bg = '/tmp/BBRC_E00080_T1.nii.gz'

@snap.plot_segment(filepaths,
bg=bg,
axes='x', @slices=[189,192,195,198],
opacity=50,
animated=True)

@# Probability maps as different channels
###
from nisnap import xnat
@xnat.plot_segment(config='/home/grg/.xnat_bsc.cfg',
resource_name='FREESURFER6_HIRES',
experiment_id='BBRC_E00080',
axes='x', @slices=range(195,204,3),
@samebox=True,
opacity=50,
contours=True,
animated=True)

@# XNAT integration
