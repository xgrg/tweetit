import pyxnat
print(pyxnat.__version__)
###
x = pyxnat.Interface(server='https://devxnat.barcelonabeta.org',
user='goperto')
###
p = x.select.project('testenv')
s = p.subject('11223')
s
###
e = s.experiments().first()
e
###
list(e.scans())
###
list(e.resources())
###
r = e.resource('BBRC_VALIDATOR')
r.download_snapshot('ASHS', '/tmp/ashs.png')
###
from IPython.display import Image
Image(filename='/tmp/ashs.png')
