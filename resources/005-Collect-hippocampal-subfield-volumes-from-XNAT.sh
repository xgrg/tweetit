import pyxnat
print(pyxnat.__version__)
###
c = pyxnat.Interface(config='/home/grg/.xnat_bsc.cfg')

@# Collect experiments from given project
project = 'ALFA_OPCIONAL'
@experiments = c.array.experiments(project_id=project,
columns=['subject_label'])
@print('%s experiments found'%len(experiments.data))
###
table = []
@# Querying FreeSurfer resource -if any- from each experiment
# Here for the example we do it only for 50 subjects.

@for e in experiments.data[:50]:
@s = int(e['subject_label'])
@r = c.select.experiment(e['ID']).resource('FREESURFER6')

@@if r.exists():
@volumes = r.hippoSfVolumes()
@# `Resource.hippoSfVolumes()` is found in branch `bbrc`
# https://github.com/xgrg/pyxnat/tree/bbrc
@@volumes['subject'] = s
@table.append(volumes)
###
# Convert to dataframe
import pandas as pd
@hippoSfVolumes = pd.concat(table).set_index('subject')
@hippoSfVolumes.sort_index().head()
###
