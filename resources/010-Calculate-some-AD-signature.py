# Let's first fetch some FreeSurfer results
import pyxnat
x = pyxnat.Interface(config='/home/grg/.xnat_bsc.cfg')
@exp = x.select.experiment('BBRC_E00080')
res = exp.resource('FREESURFER6_HIRES')
aparc = res.aparc()

@# DataFrame should look like this
aparc.query('measurement == "SurfArea"').head()
###
regions = ['entorhinal', 'inferiortemporal', 'middletemporal',
'fusiform']
@# ROIs from Jack et al., Alzheimers Dement. 2017
@# doi: 10.1016/j.jalz.2016.08.005

@@weighted_sum = 0
total_surf_area = 0

@# For each region/hemisphere
for r in regions:
for s in ['left', 'right']:

@@# Filter the DataFrame using current region/hemisphere
@roi = aparc.query('region == "%s" & side == "%s"'%(r, s))

@@# Get cortical thickness
@thickness = float(roi.query('measurement == "ThickAvg"').value)

@@# Get surface area
@surf_area = float(roi.query('measurement == "SurfArea"').value)

@@# Add them to the total sum
@weighted_sum += thickness * surf_area
@total_surf_area += surf_area

çç@@# The final result is the ratio between both values
@final = weighted_sum / total_surf_area
@final
