import pyxnat
import pandas as pd

# Let's take an experiment from XNAT
@x = pyxnat.Interface(config='/home/grg/.xnat_ci.cfg')
@e = x.select.project('testenv').subject('10163').experiment('1234567890')

@# This experiment has some FDG PET data
@r = e.resource('FDG_QUANTIFICATION')
@list(r.files())
###
# With some precomputed statistics as in
@f = r.file('quantification_results.csv')

# Let's download them
@fp = '/tmp/results.csv'
@f.get(fp)

@# Then display them in a table
@df = pd.read_csv(fp)
df.head()
###
# Filtering this data is easy
@df.query('atlas == "AAL" & '\
@'region == "Precentral_L" & '\
@'reference_region == "pons"')
###
df.query('region.str.startswith("Hippocampus") & '\
@'reference_region =="pons"')
###
# What about images?
@from nilearn import plotting, image
@f = r.file('wstatic_pet_scaled_pons.nii.gz')

@# Let's download it
fp = '/tmp/pet.nii.gz'
@f.get(fp)
@# And display it
@im = plotting.plot_epi(fp,
display_mode='z',
cut_coords=range(0,90,30))
###
# Looking at some Region-of-Interest
@roi_fp = '/tmp/MetaROIs/Composite.nii'
im = plotting.plot_roi(roi_fp,
bg_img=fp)
###
# Extract value from that ROI
import numpy as np
@data = image.load_img(fp).dataobj
@roi = image.load_img(roi_fp).dataobj
@np.mean(data[roi==1])
###
# Smoothing the image a little bit
@im = image.smooth_img(fp, fwhm=8)
@plotting.plot_roi(roi_fp, bg_img=im)
###
# Save it to hard disk
@fp = '/tmp/smoothed_data.nii.gz'
@im.to_filename('/tmp/smoothed_data.nii.gz')
###
# Extract the new value
@smoothed = image.load_img(fp).dataobj
@np.mean(smoothed[roi==1])
###
