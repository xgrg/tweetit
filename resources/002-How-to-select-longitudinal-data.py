import pyxnat
x = pyxnat.Interface(config='/home/grg/.xnat_bsc.cfg')

# We start collecting experiments from a few projects
@@experiments = []
for p in ['ALFA_PLUS', 'ALFA_OPCIONAL']:
@# For each project, collect existing experiments
@# with information e.g. subject_label, scan date
@@columns = ['subject_label', 'date']
project_exp = x.array.experiments(project_id=p, columns=columns)
@@@# Add them to a big list
@experiments.extend(project_exp.data)

@çprint('%s experiments found in both projects'%len(experiments))
###
# For each experiment, retrieve info e.g. subject label,
# acquisition date, session ID.
@subjects = {}
@for e in experiments:
subject_label = e['subject_label']
mr_scandate = e['date']
session_id = e['ID']
project_id = e['project']

@# Create a list for each subject and store the information
@@@subjects.setdefault(subject_label, [])
info = (session_id, mr_scandate, project_id)
@subjects[subject_label].append(info)

@# Now for each subject, we have the number of existing scans
# and their acquisition dates
@@çprint(subjects['10010'])
###
# Give me the ones with more than one timepoint
@@@longitudinal = {}
for subject, sessions in subjects.items():
@if len(sessions) > 1:
@longitudinal[subject] = sessions

çç# Print the results
@@for each in list(longitudinal.items())[:5]:
print(each)
@çprint(len(longitudinal))

