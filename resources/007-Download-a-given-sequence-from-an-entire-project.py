import pyxnat
x = pyxnat.Interface(server='https://bscmaragall01.bsc.es',
user='goperto',
verify=False)
###
# Define the desired sequences
sequence_names = ['pCASL', 'pCASL_SENSE']

@# Select a source project
p = x.select.project('ASLTEST')

@# Iterate through every subject/experiment/scan
@for subject in p.subjects():
@for e in subject.experiments():
@for scan in e.scans():
@@scan_type = scan.attrs.get('type')

@@# If scan type matches the desired sequences
@if not scan.label().startswith('0-') and \
@  scan_type in sequence_names:

@print(subject.label(), e.label(), \
@scan.label(), scan_type)

@@# Get files from scan
@files = scan.resource('NIFTI').files()

@for f in files:

@@# NIfTi files preferably
@if f.label().endswith('.nii.gz'):
@@fp = '/tmp/test/%s_%s.nii.gz'\
   %(s.label(), scan_type)

@# Proceed to download
f.get(fp)
###
ls /tmp/test
###
