from glob import glob
import os.path as op
import json
import random
from datetime import datetime
import time
import tweepy
from credentials import *

frequency = 1 #in days

def tweet(status, imagePath=None, doit=True):
    # OAuth process, using the keys and tokens
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # Creation of the actual interface, using authentication
    api = tweepy.API(auth)

    # Creates the user object. The me() method returns the user whose authentication keys were used.
    user = api.me()

    print('Name: ' + user.name)
    print('Location: ' + user.location)
    print('Friends: ' + str(user.followers_count))

    if doit:
        media_ids = []
        if not imagePath is None:
            for filename in imagePath:
                 res = api.media_upload(filename)
                 media_ids.append(res.media_id)

        # Send the tweet.
        api.update_status(status=status, lat=41.3891, lon=2.1904,
            media_ids=media_ids)

def next_tweet(files, last_tweet_datetime, last_tweet_id):

    print('last_tweet_datetime: %s'%last_tweet_datetime)
    print('last_tweet_id: %s'%last_tweet_id)

    l = [e for e in files if not op.split(e)[-1].startswith(last_tweet_id)]
    a = random.randint(0, len(l)-1)
    #print('random: %s'%a)
    next_tweet = op.split(l[a])[-1][:3]
    #print('next_tweet: %s'%next_tweet)
    return next_tweet

def is_it_time(last_tweet_datetime, now=None, delta=7):
    if now is None:
        now = datetime.now()
    elapsed = (now - last_tweet_datetime).days#()
    #print('How many days since last tweet? %s (last tweet: %s - now: %s)'\
    #            %(elapsed, lt, now))
    #print('Defined frequency: %s'%delta)
    return elapsed > delta

def check_tweet_lengths(tweets):
    for k, v in tweets.items():
        if len(v['body']) > 280:
            raise Exception('Tweet too long (>280 char.) %s %s (%s)'\
                    %(k, v['body'], len(v['body'])))

if __name__ == '__main__':

    j = json.load(open('history.json'))
    last_tweet_datetime = datetime.strptime(j['last_tweet_datetime'], '%Y%m%d-%H%M%S')
    last_tweet_id = j['last_tweet_id']

    while(True):

        files = sorted(glob('resources/*.txt'))
        #print(files)
        media = []
        for ext in ('jpg', 'gif', 'png'):
            media.extend(sorted(glob('resources/*.%s'%ext)))
        media_per_tweet = {}
        for e in media:
            sl = op.split(e)[-1][:3]
            media_per_tweet.setdefault(sl, []).append(e)
        #print(media_per_tweet)

        tweets = {op.split(e)[-1][:3] : {'body': open(glob('resources/%s*.txt'%op.split(e)[-1][:3])[0]).read().rstrip('\n'),
            'media': media_per_tweet.get(op.split(e)[-1][:3], [])} for e in files}

        check_tweet_lengths(tweets)
        #print('tweets: %s'%tweets)


        if not is_it_time(last_tweet_datetime, delta=frequency):
            time.sleep(1)
        else:
            nt = next_tweet(files, last_tweet_datetime, last_tweet_id)
            print('Tweet (%s): %s'%(nt, tweets[nt]))
            last_tweet_id = nt
            last_tweet_datetime = datetime.now()
            tweet(tweets[nt]['body'], tweets[nt]['media'])
